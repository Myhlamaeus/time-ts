{ pkgs ? import <nixpkgs> {} }:

with pkgs;

stdenv.mkDerivation {
  name = "fitnesspilot-web";

  buildInputs = [
    pkgconfig
    autoconf
    automake
    cmake
    nasm
    nodejs-12_x
    libtool
    libpng.dev
    pngquant
    optipng
    libjpeg.dev
    mozjpeg
    gifsicle
    libwebp
    libtiff

    gnumake
    file
    gcc

    nodejs-12_x
    nodePackages.typescript-language-server
    nodePackages.vscode-css-languageserver-bin
    nodePackages.vscode-html-languageserver-bin
  ];

  shellHook = ''
    export PATH="$PWD/node_modules/.bin/:$PATH"
  '';
}
