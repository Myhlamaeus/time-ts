import * as Eq from "fp-ts/lib/Eq"
import * as Ord from "fp-ts/lib/Ord"
import { Duration as Duration_ } from "duration-fns"
import * as dur from "duration-fns"
import * as Monoid from "fp-ts/lib/Monoid"
import { wrapOption, compareNumKeys } from "./internal"
import { Prism, Iso } from "monocle-ts"
import { flow } from "fp-ts/lib/function"
import { Torsor } from "./Torsor"

import { Time, isoTime } from "./UTC/Time"

export type Duration = {
  readonly tag: "Duration"
} & Omit<Readonly<Duration_>, "milliseconds">

const durationAsDuration_ = new Iso<Duration, Duration_>(
  ({ tag, seconds, ...dur }) => ({
    seconds: Math.floor(seconds),
    milliseconds: (seconds - Math.floor(seconds)) * 1000,
    ...dur
  }),
  ({ seconds, milliseconds, ...dur }) => ({
    tag: "Duration",
    seconds: seconds + milliseconds / 1000,
    ...dur
  })
)

export const normalise = flow(
  durationAsDuration_.get,
  dur.normalize,
  durationAsDuration_.reverseGet
)
export const normalize = normalise

export const duration: Ord.Ord<Duration> & Torsor<Time, Duration> = {
  ...Eq.getStructEq({
    years: Eq.eqNumber,
    months: Eq.eqNumber,
    weeks: Eq.eqNumber,
    days: Eq.eqNumber,
    hours: Eq.eqNumber,
    minutes: Eq.eqNumber,
    seconds: Eq.eqNumber
  }),
  compare: compareNumKeys<Duration>([
    "years",
    "months",
    "days",
    "hours",
    "minutes",
    "seconds"
  ]),
  ...Monoid.getStructMonoid({
    tag: {
      concat: (_: "Duration", __: "Duration") => "Duration",
      empty: "Duration"
    } as Monoid.Monoid<"Duration">,
    years: Monoid.monoidSum,
    months: Monoid.monoidSum,
    weeks: Monoid.monoidSum,
    days: Monoid.monoidSum,
    hours: Monoid.monoidSum,
    minutes: Monoid.monoidSum,
    seconds: Monoid.monoidSum
  }),
  inverse: ({ tag, years, months, weeks, days, hours, minutes, seconds }) => ({
    tag,
    years: -years,
    months: -months,
    weeks: -weeks,
    days: -days,
    hours: -hours,
    minutes: -minutes,
    seconds: -seconds
  }),
  add: (t, d) =>
    isoTime.reverseGet(dur.apply(isoTime.get(t), durationAsDuration_.get(d))),
  difference: (t, t2) =>
    durationAsDuration_.reverseGet(dur.between(isoTime.get(t), isoTime.get(t2)))
}

export const equals = (d2: Duration) => (d: Duration) => duration.equals(d, d2)
export const compare = (d2: Duration) => (d: Duration) =>
  duration.compare(d, d2)
export const concat = (d2: Duration) => (d: Duration) => duration.concat(d, d2)
export const empty = duration.empty
export const inverse = duration.inverse
export const add = (d: Duration) => (t: Time) => duration.add(t, d)
export const difference = (t2: Time) => (t: Time) => duration.difference(t, t2)

const isoStringAsDuration_ = new Prism<string, Duration_>(
  wrapOption(dur.parse),
  dur.toString
)
export const isoStringAsDuration = isoStringAsDuration_.composeIso(
  durationAsDuration_.reverse()
)
