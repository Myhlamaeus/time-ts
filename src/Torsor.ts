import { Group } from "fp-ts/lib/Group"

export interface Torsor<p, v> extends Group<v> {
  readonly add: (p: p, v: v) => p
  readonly difference: (p: p, p2: p) => v
}
