import * as Opt from "fp-ts/lib/Option"
import * as Ord from "fp-ts/lib/Ord"
import * as Ordering from "fp-ts/lib/Ordering"
import * as Arr from "fp-ts/lib/ReadonlyArray"
import * as Func from "fp-ts/lib/function"
import { flow } from "fp-ts/lib/function"
import { Prism, Getter } from "monocle-ts"

export const on = <a, b>(f: (a: a) => b) => <c>(g: (b: b, b2: b) => c) => (
  a: a,
  a2: a
) => g(f(a), f(a2))

export const wrapOption = <f extends (...args: any[]) => any>(f: f) => (
  ...args: Parameters<f>
) => Opt.tryCatch<ReturnType<f>>(() => f(...args))

export const compareNumKeys = <a extends Record<any, any>>(ks: (keyof a)[]) => (
  a: a,
  a2: a
) =>
  flow(
    Arr.map((k: keyof a) => Ord.ordNumber.compare(a[k], a2[k])),
    vs => Arr.readonlyArray.foldMap(Ordering.monoidOrdering)(vs, Func.identity)
  )(ks)

export const prismToGetter = <s, a>(prism: Prism<s, a>) =>
  new Getter(prism.reverseGet)
