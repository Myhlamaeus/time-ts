import * as Ord from "fp-ts/lib/Ord"
import * as Opt from "fp-ts/lib/Option"
import * as Func from "fp-ts/lib/function"
import * as IO from "fp-ts/lib/IO"
import { Prism } from "monocle-ts"
import * as D from "date-fns/fp"
import * as D2 from "fp-ts/lib/Date"
import { Newtype, iso } from "newtype-ts"

import { on, wrapOption } from "../internal"

export interface Time extends Newtype<{ readonly Time: unique symbol }, Date> {}
export const isoTime = iso<Time>()

export const dateAsTime = new Prism<Date, Date>(
  Opt.fromPredicate(d => !isNaN(d.getTime())),
  Func.identity
).composeIso(isoTime.reverse())
const liftDate = on(isoTime.get)

export const time: Ord.Ord<Time> = {
  equals: liftDate(Ord.ordDate.equals),
  compare: liftDate(Ord.ordDate.compare)
}

export const equals = (d2: Time) => (d: Time) => time.equals(d, d2)
export const compare = (d2: Time) => (d: Time) => time.compare(d, d2)

export const now = IO.io.map(D2.create, isoTime.reverseGet)

const isoStringAsDate = new Prism<string, Date>(
  wrapOption(D.parseISO),
  D.formatISO
)
export const isoStringAsTime = isoStringAsDate.composeIso(isoTime.reverse())
