import * as Eq from "fp-ts/lib/Eq"
import * as Ord from "fp-ts/lib/Ord"
import * as Opt from "fp-ts/lib/Option"
import * as Arr from "fp-ts/lib/ReadonlyArray"
import * as IO from "fp-ts/lib/IO"
import { Prism, Getter } from "monocle-ts"
import { flow } from "fp-ts/lib/function"

import { dateAsTime, now } from "./Time"
import { compareNumKeys, prismToGetter } from "../internal"

export type Day = {
  readonly type: "Day"
  readonly year: number
  readonly month: number
  readonly day: number
}

export const day: Ord.Ord<Day> = {
  ...Eq.getStructEq({
    year: Eq.eqNumber,
    month: Eq.eqNumber,
    day: Eq.eqNumber
  }),
  compare: compareNumKeys<Day>(["year", "month", "day"])
}

export const equals = (d2: Day) => (d: Day) => day.equals(d, d2)
export const compare = (d2: Day) => (d: Day) => day.compare(d, d2)

export const dayOfTime = prismToGetter(dateAsTime).compose(
  new Getter<Date, Day>((t: Date) => ({
    type: "Day",
    year: t.getUTCFullYear(),
    month: t.getUTCMonth() + 1,
    day: t.getUTCDate()
  }))
)

export const today = IO.io.map(now, dayOfTime.get)

export const mkDay = (year: number, month: number, day: number) =>
  flow(
    dateAsTime.getOption,
    Opt.map(dayOfTime.get)
  )(new Date(Date.UTC(year, month, day)))

const regex = /^(-?\d+)-(\d{2})-(\d{2})$/
export const isoStringAsDay = new Prism<string, Day>(
  flow(
    s => s.match(regex),
    Opt.fromNullable,
    Opt.map(Arr.map(Number)),
    Opt.chain(([, y, m, d]) =>
      Opt.chain(
        Opt.fromPredicate(
          ({ year, month, day }: Day) => year === y && month === m && day === d
        )
      )(mkDay(y, m - 1, d))
    )
  ),
  ({ year, month, day }) =>
    `${String(year).padStart(4, "0")}-${String(month).padStart(
      2,
      "0"
    )}-${String(day).padStart(2, "0")}`
)
