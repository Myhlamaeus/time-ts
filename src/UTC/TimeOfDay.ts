import * as Eq from "fp-ts/lib/Eq"
import * as Ord from "fp-ts/lib/Ord"
import * as Opt from "fp-ts/lib/Option"
import * as Arr from "fp-ts/lib/ReadonlyArray"
import * as IO from "fp-ts/lib/IO"
import { Prism, Getter, Iso } from "monocle-ts"
import { flow } from "fp-ts/lib/function"

import { dateAsTime, now, isoTime, Time } from "./Time"
import { compareNumKeys, prismToGetter } from "../internal"
import { dayOfTime, Day } from "./Day"

export type TimeOfDay = {
  readonly type: "TimeOfDay"
  readonly hour: number
  readonly minute: number
  readonly second: number
}

export const timeOfDay: Ord.Ord<TimeOfDay> = {
  ...Eq.getStructEq({
    hour: Eq.eqNumber,
    minute: Eq.eqNumber,
    second: Eq.eqNumber
  }),
  compare: compareNumKeys<TimeOfDay>(["hour", "minute", "second"])
}

export const equals = (d2: TimeOfDay) => (d: TimeOfDay) =>
  timeOfDay.equals(d, d2)
export const compare = (d2: TimeOfDay) => (d: TimeOfDay) =>
  timeOfDay.compare(d, d2)

export const timeOfDayOfTime = prismToGetter(dateAsTime).compose(
  new Getter<Date, TimeOfDay>((t: Date) => ({
    type: "TimeOfDay",
    hour: t.getUTCHours(),
    minute: t.getUTCMinutes(),
    second: t.getUTCSeconds() + t.getUTCMilliseconds() / 1000
  }))
)

export const currentTimeOfDay = IO.io.map(now, timeOfDayOfTime.get)

export const mkTimeOfDay = (hour: number, minute: number, second: number) =>
  Opt.fromPredicate(
    ({ hour, minute, second }: TimeOfDay) =>
      hour >= 0 &&
      hour < 24 &&
      minute >= 0 &&
      minute < 60 &&
      second >= 0 &&
      second <= 60
  )({
    type: "TimeOfDay",
    hour,
    minute,
    second
  })

const regex = /^(\d{2}):(\d{2})(?::(\d{2}(?:\.(\d+))?))?Z$/
const pad = (n: number) => String(n).padStart(2, "0")
export const isoStringAsTimeOfDay = new Prism<string, TimeOfDay>(
  flow(
    s => s.match(regex),
    Opt.fromNullable,
    Opt.map(([_, h, m, s = "0"]) => [h, m, s]),
    Opt.map(Arr.map(Number)),
    Opt.chain(([h, m, s]) => mkTimeOfDay(h, m, s))
  ),
  ({ hour, minute, second }) => `${pad(hour)}:${pad(minute)}:${pad(second)}Z`
)

export const timeFromDayAndTimeOfDay = (d: Day, tod: TimeOfDay) => {
  const date = new Date(
    Date.UTC(
      d.year,
      d.month,
      d.day,
      tod.hour,
      tod.minute,
      Math.floor(tod.second),
      (tod.second - Math.floor(tod.second)) * 1000
    )
  )

  return isoTime.reverseGet(date)
}

export const timeAsDayAndTimeOfDayPair = new Iso<Time, [Day, TimeOfDay]>(
  t => [dayOfTime.get(t), timeOfDayOfTime.get(t)],
  ([d, tod]) => timeFromDayAndTimeOfDay(d, tod)
)
