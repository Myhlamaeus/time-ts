import * as Ord from "fp-ts/lib/Ord"
import * as IO from "fp-ts/lib/IO"
import { Newtype, iso } from "newtype-ts"

import { on } from "../internal"

export interface Local<a>
  extends Newtype<{ readonly Local: unique symbol }, a> {}
export const isoLocal = <a>() => iso<Local<a>>()
const liftToLocal = <a>() => on(isoLocal<a>().get)

export const local = <a>(ord: Ord.Ord<a>): Ord.Ord<Local<a>> => ({
  equals: liftToLocal<a>()(ord.equals),
  compare: liftToLocal<a>()(ord.compare)
})

export const equals = <a>(ord: Ord.Ord<a>) => {
  const local_ = local(ord)

  return (d2: Local<a>) => (d: Local<a>) => local_.equals(d, d2)
}
export const compare = <a>(ord: Ord.Ord<a>) => {
  const local_ = local(ord)

  return (d2: Local<a>) => (d: Local<a>) => local_.compare(d, d2)
}

export const currentTimeZone: IO.IO<string> = () =>
  new Intl.DateTimeFormat().resolvedOptions().timeZone
