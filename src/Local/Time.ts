import * as DTz from "date-fns-tz/fp"
import { flow } from "fp-ts/lib/function"
import { Iso } from "monocle-ts"
import * as IO from "fp-ts/lib/IO"

import {
  Local,
  local,
  equals as equalsLocal,
  compare as compareLocal,
  isoLocal
} from "./Local"
import { Time, time, isoTime, now, isoStringAsTime } from "../UTC/Time"
import { dayOfTime } from "../UTC"

export type LocalTime = Local<Time>
export const isoLocalTime = isoLocal<Time>()

export const localTime = local(time)

export const equals = equalsLocal(time)
export const compare = compareLocal(time)

export const timeAsLocalTime = (timeZone: string) =>
  new Iso<Time, LocalTime>(
    flow(
      isoTime.get,
      DTz.utcToZonedTime(timeZone),
      isoTime.reverseGet,
      isoLocalTime.reverseGet
    ),
    flow(
      isoLocalTime.get,
      isoTime.get,
      DTz.zonedTimeToUtc(timeZone),
      isoTime.reverseGet
    )
  )

export const localNow = (timeZone: string) =>
  IO.io.map(now, timeAsLocalTime(timeZone).get)

export const isoStringAsLocalTime = new Iso<string, string>(
  s => `${s}Z`,
  s => s.replace(/Z$/, "")
)
  .composePrism(isoStringAsTime)
  .composeIso(isoLocalTime.reverse())

export const dayOfLocalTime = isoLocalTime.composeGetter(dayOfTime)
