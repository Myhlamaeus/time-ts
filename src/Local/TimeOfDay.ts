import {
  Local,
  local,
  equals as equalsLocal,
  compare as compareLocal,
  isoLocal
} from "./Local"
import * as Opt from "fp-ts/lib/Option"
import * as IO from "fp-ts/lib/IO"
import { flow } from "fp-ts/lib/function"
import {
  TimeOfDay,
  timeOfDay,
  timeOfDayOfTime,
  mkTimeOfDay,
  isoStringAsTimeOfDay,
  timeFromDayAndTimeOfDay
} from "../UTC/TimeOfDay"
import { isoLocalTime, localNow, LocalTime, dayOfLocalTime } from "./Time"
import { Iso } from "monocle-ts"
import { Day } from "../UTC"

export type LocalTimeOfDay = Local<TimeOfDay>
export const isoLocalTimeOfDay = isoLocal<TimeOfDay>()

export const localTimeOfDay = local(timeOfDay)

export const equals = equalsLocal(timeOfDay)
export const compare = compareLocal(timeOfDay)

export const localTimeOfDayOfLocalTime = isoLocalTime
  .composeGetter(timeOfDayOfTime)
  .composeIso(isoLocalTimeOfDay.reverse())

export const currentLocalTimeOfDay = (timeZone: string) =>
  IO.io.map(localNow(timeZone), localTimeOfDayOfLocalTime.get)

export const mkLocalTimeOfDay = flow(
  mkTimeOfDay,
  Opt.map(isoLocalTimeOfDay.reverseGet)
)

export const isoStringAsLocalTimeOfDay = new Iso<string, string>(
  s => `${s}Z`,
  s => s.replace(/Z$/, "")
)
  .composePrism(isoStringAsTimeOfDay)
  .composeIso(isoLocalTimeOfDay.reverse())

export const localTimeFromDayAndLocalTimeOfDay = (
  d: Day,
  tod: LocalTimeOfDay
) =>
  isoLocalTime.reverseGet(
    timeFromDayAndTimeOfDay(d, isoLocalTimeOfDay.get(tod))
  )

export const localTimeAsDayAndLocalTimeOfDayPair = new Iso<
  LocalTime,
  [Day, LocalTimeOfDay]
>(
  t => [dayOfLocalTime.get(t), localTimeOfDayOfLocalTime.get(t)],
  ([d, tod]) => localTimeFromDayAndLocalTimeOfDay(d, tod)
)
